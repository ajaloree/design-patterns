package com.anoop.dp.adapter;

public class WildTurkey implements Turkey {

	@Override
	public void gobble() {
		System.out.println("Wild Turkey's Gobble!");
	}

	@Override
	public void fly() {
		System.out.println("Wild Turkey's Flying!");
	}

}
