package com.anoop.dp.adapter;

public interface Duck {

	public void quack();
	
	public void fly();
}
