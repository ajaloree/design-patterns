package com.anoop.dp.adapter;

public class DuckTestDrive {

	public static void main(String[] args) {
		
		Duck md = new MallardDuck();
		
		Turkey wt = new WildTurkey();
		
		Duck pseudoDuck = new TurkeyAdapter(wt);
		
		md.fly();
		
		wt.fly();
		
		pseudoDuck.fly();
		
	}
	
}
