package com.anoop.dp.adapter;

public class MallardDuck implements Duck {

	@Override
	public void quack() {
		System.out.println("Mallard Duck's Quack !!");
	}

	@Override
	public void fly() {
		System.out.println("Mallard Duck's Magnificient Flyiing !!");
	}

}
