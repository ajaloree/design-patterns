package com.anoop.dp.factory_method;

import com.anoop.dp.factory_method.creator.ChicagoStylePizzaStore;
import com.anoop.dp.factory_method.creator.NYPizzaStore;
import com.anoop.dp.factory_method.creator.PizzaStore;
import com.anoop.dp.factory_method.product.Pizza;

public class PizzaTestDrive {

	public static void main(String[] args) {
		
		PizzaStore nyStore = new NYPizzaStore(); 
		PizzaStore chicagoStore = new ChicagoStylePizzaStore();
		
		Pizza pizza = nyStore.orderPizza("cheese"); 
		System.out.println("Ethan ordered a " + pizza.getName() + "\n");
		
		pizza = chicagoStore.orderPizza("cheese");
		System.out.println("Joel ordered a " + pizza.getName() + "\n");
	}
	
}
