package com.anoop.dp.factory_method.product;

public class ChicagoStylePepperoniPizza extends Pizza {
	
	public ChicagoStylePepperoniPizza() {
		name = "Chicago Style Sauce and Pepperoni Pizza"; 
		dough = "Thin Crust Dough";
		sauce = "Carbonara Sauce";
		toppings.add("Pepporoni"); 
	}
}