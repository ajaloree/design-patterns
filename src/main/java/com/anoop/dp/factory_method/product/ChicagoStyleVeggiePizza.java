package com.anoop.dp.factory_method.product;

public class ChicagoStyleVeggiePizza extends Pizza {
	
	public ChicagoStyleVeggiePizza() {
		name = "Chicago Style Sauce and Veggie Pizza"; 
		dough = "Thin Crust Dough";
		sauce = "Tomato Sauce";
		toppings.add("Capsicum"); 
		toppings.add("Onion"); 
		toppings.add("Olives"); 
	}
}