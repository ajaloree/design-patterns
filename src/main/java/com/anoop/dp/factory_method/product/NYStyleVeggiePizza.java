package com.anoop.dp.factory_method.product;

public class NYStyleVeggiePizza extends Pizza {
	
	public NYStyleVeggiePizza() {
		name = "NY Style Sauce and Veggie Pizza"; 
		dough = "Thin Crust Dough";
		sauce = "Tomato Sauce";
		toppings.add("Capsicum"); 
		toppings.add("Onion"); 
		toppings.add("Olives"); 
	}
}