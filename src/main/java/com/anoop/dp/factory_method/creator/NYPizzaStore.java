package com.anoop.dp.factory_method.creator;

import com.anoop.dp.factory_method.product.NYStyleCheesePizza;
import com.anoop.dp.factory_method.product.NYStylePepperoniPizza;
import com.anoop.dp.factory_method.product.NYStyleVeggiePizza;
import com.anoop.dp.factory_method.product.Pizza;

public class NYPizzaStore extends PizzaStore {

	@Override
	public Pizza createPizza(String type) {
		
		if(type.equals("cheese")) {
			return new NYStyleCheesePizza();
		} else if(type.equals("veggie")) {
			return new NYStyleVeggiePizza();
		} else if(type.equals("pepperoni")) {
			return new NYStylePepperoniPizza();
		} else {
			return null;
		}
	}

}
