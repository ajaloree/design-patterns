package com.anoop.dp.factory_method.creator;

import com.anoop.dp.factory_method.product.ChicagoStyleCheesePizza;
import com.anoop.dp.factory_method.product.ChicagoStylePepperoniPizza;
import com.anoop.dp.factory_method.product.ChicagoStyleVeggiePizza;
import com.anoop.dp.factory_method.product.Pizza;

public class ChicagoStylePizzaStore extends PizzaStore {

	@Override
	public Pizza createPizza(String type) {
		
		if(type.equals("cheese")) {
			return new ChicagoStyleCheesePizza();
		} else if(type.equals("veggie")) {
			return new ChicagoStyleVeggiePizza();
		} else if(type.equals("pepperoni")) {
			return new ChicagoStylePepperoniPizza();
		} else {
			return null;
		}
	}

}
