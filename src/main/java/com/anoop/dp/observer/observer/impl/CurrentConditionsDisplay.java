package com.anoop.dp.observer.observer.impl;

import com.anoop.dp.observer.observer.DisplayElement;
import com.anoop.dp.observer.observer.Observer;
import com.anoop.dp.observer.subject.impl.WeatherData;

public class CurrentConditionsDisplay implements Observer, DisplayElement {
	
	private WeatherData wd;
	private float temp;
	private float humidity;
	
	public CurrentConditionsDisplay(WeatherData wd) {
		this.wd = wd;
		wd.registerObserver(this);
	}
	
	@Override
	public void update(float temp, float humidity, float pressure) {
		this.temp = temp;
		this.humidity = humidity;
		display();
	}

	@Override
	public void display() {
		System.out.println("Current conditions: " + temp+ "F degrees and " + humidity + "% humidity");
	}

}
