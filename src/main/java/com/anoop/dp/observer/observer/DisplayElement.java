package com.anoop.dp.observer.observer;

public interface DisplayElement {

	public void display();
	
}
