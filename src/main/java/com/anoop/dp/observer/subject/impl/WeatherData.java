package com.anoop.dp.observer.subject.impl;

import java.util.ArrayList;
import java.util.List;

import com.anoop.dp.observer.observer.Observer;
import com.anoop.dp.observer.subject.Subject;

public class WeatherData implements Subject {
	
	private List<Observer> observers;
	private float temp; 
	private float humidity; 
	private float pressure;
	
	public WeatherData() {
		observers = new ArrayList<>();
	}
	
	@Override
	public void registerObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void notifyObservers() {
		observers.forEach(o -> {
			o.update(temp, humidity, pressure);
		});
	}
	
	public void measurementsChanged() {
		notifyObservers();
	}
	
	public void setMeasurements(float temperature, float humidity, float pressure) { 
		this.temp = temperature;
		this.humidity = humidity;
		this.pressure = pressure;
		notifyObservers();
	}

}
