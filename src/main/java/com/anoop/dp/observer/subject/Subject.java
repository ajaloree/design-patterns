package com.anoop.dp.observer.subject;

import com.anoop.dp.observer.observer.Observer;

public interface Subject {

	public void registerObserver(Observer o); 
	
	public void removeObserver(Observer o); 
	
	public void notifyObservers();
	
}