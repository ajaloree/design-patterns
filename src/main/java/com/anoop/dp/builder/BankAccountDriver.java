package com.anoop.dp.builder;

public class BankAccountDriver {

	public static void main(String[] args) {

		BankAccount account = new BankAccount.Builder(1234L)
	            			.withOwner("Marge")
	            			.withBranch("Springfield")
	            			.withBalance(100)
	            			.withInterestRate(2.5)
	            			.build();

		BankAccount anotherAccount = new BankAccount.Builder(4567L)
	            					.withOwner("Homer")
	            					.withBranch("Springfield")
	            					.withBalance(100)
	            					.withInterestRate(2.5)
	            					.build();
		
		System.out.println(account);
		System.out.println(anotherAccount);
	}

}
