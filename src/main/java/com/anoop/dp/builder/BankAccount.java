package com.anoop.dp.builder;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BankAccount {

    private long accountNumber; 
    private String owner;
    private String branch;
    private double balance;
    private double interestRate;
	    
    public static class Builder {
    	
        private long accountNumber; //This is important, so we'll pass it to the constructor.
        private String owner;
        private String branch;
        private double balance;
        private double interestRate;
        
        public Builder(long accountNumber) {
        	this.accountNumber = accountNumber;
        }
        
        public Builder withOwner(String owner) {
        	this.owner = owner;
        	return this;
        }
        
        public Builder withBranch(String branch) {
        	this.branch = branch;
        	return this;
        }
        
        public Builder withBalance(double balance) {
        	this.balance = balance;
        	return this;
        }
        
        public Builder withInterestRate(double interestRate) {
        	this.interestRate = interestRate;
        	return this;
        }
        
        public BankAccount build() {
        	BankAccount ba = new BankAccount();
        	ba.accountNumber = this.accountNumber;
        	ba.owner = this.owner;
        	ba.branch = this.branch;
        	ba.balance = this.balance;
        	ba.interestRate = this.interestRate;
        	return ba;
        }
    }
}