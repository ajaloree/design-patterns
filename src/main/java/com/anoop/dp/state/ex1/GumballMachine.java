package com.anoop.dp.state.ex1;

import com.anoop.dp.state.ex1.states.GumballMachineState;
import com.anoop.dp.state.ex1.states.HasQuarterState;
import com.anoop.dp.state.ex1.states.NoQuarterState;
import com.anoop.dp.state.ex1.states.SoldOutState;
import com.anoop.dp.state.ex1.states.SoldState;
import com.anoop.dp.state.ex1.states.WinnerState;

public class GumballMachine {

	GumballMachineState soldOutState; 
	GumballMachineState noQuarterState; 
	GumballMachineState hasQuarterState; 
	GumballMachineState soldState;
	GumballMachineState winnerState;
	
	GumballMachineState state; 
	
	int count = 0;
	
	public GumballMachine(int numberOfGumballs) {
		
		soldOutState = new SoldOutState(this);
		noQuarterState = new NoQuarterState(this);
		hasQuarterState = new HasQuarterState(this);
		soldState = new SoldState(this);
		winnerState = new WinnerState(this);
		
		this.count = numberOfGumballs;
		
		if(numberOfGumballs > 0) {
			state = noQuarterState;
		} else {
			state = soldOutState;
		}
	}
	
	public void setState(GumballMachineState state) {
		this.state = state;
	}
	
	public void insertQuarter() {
		state.insertQuarter();
	}
	
	public void ejectQuarter() {
		state.ejectQuarter();
	}
	
	public void turnCrank() {
		state.turnCrank();
		state.dispense();
	}
	
	public void dispense() {
		
	}
	
	public void releaseBall() {
		System.out.println("A gumball comes rolling out of the sot....");
		if(count > 0) {
			count--;
		}
	}

	public GumballMachineState getSoldOutState() {
		return soldOutState;
	}

	public GumballMachineState getNoQuarterState() {
		return noQuarterState;
	}

	public GumballMachineState getHasQuarterState() {
		return hasQuarterState;
	}

	public GumballMachineState getSoldState() {
		return soldState;
	}

	public GumballMachineState getWinnerState() {
		return winnerState;
	}

	public GumballMachineState getState() {
		return state;
	}

	public int getCount() {
		return count;
	}
	
	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder();
		sb.append("\n----------------------------------\n");
		sb.append("\nMachine State: ").append(this.state.getClass().getSimpleName());
		sb.append("\nGumballs Inventory: ").append(this.getCount());
		sb.append("\n----------------------------------\n");
		
		return sb.toString();
	}
}