package com.anoop.dp.state.ex1.states;

import com.anoop.dp.state.ex1.GumballMachine;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class NoQuarterState implements GumballMachineState {
	
	private final GumballMachine machine;
	
	@Override
	public void insertQuarter() {
		System.out.println("You inserted a quarter");
		machine.setState(machine.getHasQuarterState());
	}

	@Override
	public void ejectQuarter() {
		System.out.println("You haven't inserted a quarter");
	}

	@Override
	public void turnCrank() {
		System.out.println("You turned the crank, but there is no quarter");
	}

	@Override
	public void dispense() {
		System.out.println("You need to pay first");
	}

}
