package com.anoop.dp.state.ex1.states;

public interface GumballMachineState {
	
	public void insertQuarter();
	
	public void ejectQuarter();
	
	public void turnCrank();
	
	public void dispense();

}
