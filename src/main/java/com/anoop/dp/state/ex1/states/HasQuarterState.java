package com.anoop.dp.state.ex1.states;

import java.util.Random;

import com.anoop.dp.state.ex1.GumballMachine;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class HasQuarterState implements GumballMachineState {

	private final GumballMachine machine;
	private final Random randomWinner = new Random(System.currentTimeMillis());
	
	@Override
	public void insertQuarter() {
		System.out.println("You can't insert another quarter");
	}

	@Override
	public void ejectQuarter() {
		System.out.println("Quarter returned");
		machine.setState(machine.getNoQuarterState());
	}

	@Override
	public void turnCrank() {
		System.out.println("You turned the crank...");
		int winner = randomWinner.nextInt(10);
		if((winner == 0) && (machine.getCount() > 1)) {
			machine.setState(machine.getWinnerState());
		} else {
			machine.setState(machine.getSoldState());
		}
	}

	@Override
	public void dispense() {
		System.out.println("No gumball dispensed");
	}

}
