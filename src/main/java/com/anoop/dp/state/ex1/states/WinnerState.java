package com.anoop.dp.state.ex1.states;

import com.anoop.dp.state.ex1.GumballMachine;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class WinnerState implements GumballMachineState {

	private final GumballMachine machine;

	@Override
	public void insertQuarter() {
		System.out.println("Please wait, we are already giving you a gumball");
	}

	@Override
	public void ejectQuarter() {
		System.out.println("Sorry you aready turned the crank");
	}

	@Override
	public void turnCrank() {
		System.out.println("Turning twice doesn't get you another gumball");
	}

	@Override
	public void dispense() {
		machine.releaseBall();
		if(machine.getCount() == 0) {
			machine.setState(machine.getSoldOutState());
		} else {
			machine.releaseBall(); // If we have a second gumball - release it as the winning prize!
			System.out.println("You're a WINNER! You got two gumballs for your quarter");
			if(machine.getCount() > 0) {
				machine.setState(machine.getNoQuarterState());
			} else {
				System.out.println("Oops, out of gumballs!");
				machine.setState(machine.getSoldOutState());
			}
		}
	}
}
