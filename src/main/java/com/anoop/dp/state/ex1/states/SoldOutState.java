package com.anoop.dp.state.ex1.states;

import com.anoop.dp.state.ex1.GumballMachine;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SoldOutState implements GumballMachineState {

	private final GumballMachine machine;

	@Override
	public void insertQuarter() {
		System.out.println("You can't insert a quarter, the machine is sold out");
	}

	@Override
	public void ejectQuarter() {
		System.out.println("You can't eject the quarter as you haven't inserted one");
	}

	@Override
	public void turnCrank() {
		System.out.println("You turned the crank, but there are no gumballs");
	}

	@Override
	public void dispense() {
		System.out.println("No gumball dispensed");
	}

}
