package com.anoop.dp.state.ex1;

public class GumballMachineTestDrive {

	public static void main(String[] args) {

		GumballMachine gm = new GumballMachine(3);
		
		System.out.println(gm.toString());
		
		gm.insertQuarter();
		gm.turnCrank();
		
		System.out.println(gm.toString());

		gm.insertQuarter();
		gm.turnCrank();

		gm.insertQuarter();
		gm.turnCrank();

		System.out.println(gm.toString());

		gm.insertQuarter();
		gm.turnCrank();

		System.out.println(gm.toString());

		gm.turnCrank();

	}

}
