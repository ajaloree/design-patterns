package com.anoop.dp.state.ex2.states;

import com.anoop.dp.state.ex2.ui.Player;

public abstract class State {
	
	protected final Player player;
	
	public State(Player player) {
		this.player = player;
	}
	
    public abstract String onLock();
    
    public abstract String onPlay();
    
    public abstract String onNext();
    
    public abstract String onPrevious();

}