package com.anoop.dp.state.ex2.states;

import com.anoop.dp.state.ex2.ui.Player;

public class PlayingState extends State {

	public PlayingState(Player player) {
		super(player);
		player.setPlaying(false);
	}
	
	@Override
	public String onLock() {
		player.changeState(new ReadyState(player));
		player.setCurrentTrackAfterStop();
		return "Stop playing";
	}

	@Override
	public String onPlay() {
		player.changeState(new ReadyState(player));
		return "Paused...";
	}

	@Override
	public String onNext() {
		return player.nextTrack();
	}

	@Override
	public String onPrevious() {
		return player.previousTrack();
	}

}
