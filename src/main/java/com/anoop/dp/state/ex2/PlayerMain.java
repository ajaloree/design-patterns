package com.anoop.dp.state.ex2;

import com.anoop.dp.state.ex2.ui.Player;
import com.anoop.dp.state.ex2.ui.UI;

public class PlayerMain {

	public static void main(String[] args) {
		
        Player player = new Player();
        UI ui = new UI(player);
        ui.init();
		
	}
}
