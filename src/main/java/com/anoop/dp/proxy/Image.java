package com.anoop.dp.proxy;

public interface Image {
	
	public void displayImage();
	
}
