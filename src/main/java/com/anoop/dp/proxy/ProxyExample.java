package com.anoop.dp.proxy;

public class ProxyExample {

	public static void main(String[] args) {
		
		ProxyImage p1 = new ProxyImage("High_Res_Image_Tiger_001");
		ProxyImage p2 = new ProxyImage("High_Res_Image_Tiger_002");
		p1.displayImage();
		p2.displayImage();
		p1.displayImage();
		p2.displayImage();
	}
}
