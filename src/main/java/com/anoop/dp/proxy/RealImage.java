package com.anoop.dp.proxy;

public class RealImage implements Image {

	private final String filename;
	
	public RealImage(String filename) {
		this.filename = filename;
		loadImageFromDisk();
	}
	
	private void loadImageFromDisk() {
		for(int i = 0; i < 5; i++) {
			System.out.println("Loading image titled "+filename+" from disk...");
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Image titled "+filename+" loaded successfully from disk...");
	}
	
	@Override
	public void displayImage() {
		System.out.println("Displaying image "+filename);
		
	}
}