package com.anoop.dp.singleton;

public class SingletonDCL {
	
	private volatile static SingletonDCL instance;
	
	private SingletonDCL() {
		
	}

	public static SingletonDCL getInstance() {
		if(instance == null) {
			synchronized (SingletonDCL.class) {
				if(instance == null) {
					instance = new SingletonDCL();
				}
			}
		}
		return instance;
	}
	
	public void doSomething() {
		System.out.println("Hello world from a singleton written using DCL!");
	}
}
