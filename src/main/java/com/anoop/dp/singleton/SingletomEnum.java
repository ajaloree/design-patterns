package com.anoop.dp.singleton;

public enum SingletomEnum {
	
	INSTANCE;
	
	public void doSomething() {
		System.out.println("Hello world from a singleton written using enum!");
	}

}
