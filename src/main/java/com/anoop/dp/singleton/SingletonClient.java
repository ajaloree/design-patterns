package com.anoop.dp.singleton;

public class SingletonClient {

	public static void main(String[] args) {
		
		SingletonDCL.getInstance().doSomething();
		
		SingletomEnum.INSTANCE.doSomething();
		
	}
	
}
