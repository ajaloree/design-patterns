package com.anoop.dp.visitor;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Engine implements CarElement {
	
	private final double volume;
	private final double strokeLength;
	
	@Override
	public void accept(CarElementVisitor visitor) {
		visitor.visit(this);
	}

}
