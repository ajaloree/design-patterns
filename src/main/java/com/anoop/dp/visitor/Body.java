package com.anoop.dp.visitor;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Body implements CarElement {
	
	private final String colour;
	private final double length;
	private final double width;
	
	@Override
	public void accept(CarElementVisitor visitor) {
		visitor.visit(this);
	}

}