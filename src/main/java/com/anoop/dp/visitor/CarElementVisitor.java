package com.anoop.dp.visitor;

public interface CarElementVisitor {
	
	public void visit(Car car);
	
	public void visit(Body body);

	public void visit(Engine engine);

	public void visit(Wheel wheel);
	
}
