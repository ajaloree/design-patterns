package com.anoop.dp.visitor;

import java.util.List;

import com.anoop.dp.visitor.visitors.CarInspectionVisitor;
import com.anoop.dp.visitor.visitors.CarServicingVisitor;

public class VisitorDemo {
	
	public static void main(String[] args) {
		
		Car car = new Car(new Body("white", 200, 75), 
						  new Engine(75, 20), 
						  List.of(new Wheel("front-right"), 
								  new Wheel("front-left"), 
								  new Wheel("rear-right"), 
								  new Wheel("rear-left")));
		
		CarInspectionVisitor inspector = new CarInspectionVisitor();
		CarServicingVisitor servicer = new CarServicingVisitor();
		
		System.out.println("Asking inspector to create inspection report....");
		car.accept(inspector);
		
		System.out.println("Asking servicer to service the car....");
		car.accept(servicer);

	}

}
