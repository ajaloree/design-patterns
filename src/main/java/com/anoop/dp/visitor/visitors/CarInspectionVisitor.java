package com.anoop.dp.visitor.visitors;

import com.anoop.dp.visitor.Body;
import com.anoop.dp.visitor.Car;
import com.anoop.dp.visitor.CarElementVisitor;
import com.anoop.dp.visitor.Engine;
import com.anoop.dp.visitor.Wheel;

public class CarInspectionVisitor implements CarElementVisitor {

	@Override
	public void visit(Car car) {
		System.out.println("Inspecting eletrics, oil and coolent levels.");
	}

	@Override
	public void visit(Body body) {
		System.out.println("Inspecting body for dents and scratches.");
	}

	@Override
	public void visit(Engine engine) {
		System.out.println("Inspecting engine for wear and tear.");		
	}

	@Override
	public void visit(Wheel wheel) {
		System.out.println("Inspecting wheel tread depth and surface condition.");		
	}

}
