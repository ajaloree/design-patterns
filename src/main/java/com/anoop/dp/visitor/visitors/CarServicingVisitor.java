package com.anoop.dp.visitor.visitors;

import com.anoop.dp.visitor.Body;
import com.anoop.dp.visitor.Car;
import com.anoop.dp.visitor.CarElementVisitor;
import com.anoop.dp.visitor.Engine;
import com.anoop.dp.visitor.Wheel;

public class CarServicingVisitor implements CarElementVisitor {

	@Override
	public void visit(Car car) {
		System.out.println("Changing damaged eletric cables, top-up oil and coolent levels.");
	}

	@Override
	public void visit(Body body) {
		System.out.println("Repairing dents and scratches on the body.");
	}

	@Override
	public void visit(Engine engine) {
		System.out.println("Servicing engine and changing piston ring.");		
	}

	@Override
	public void visit(Wheel wheel) {
		System.out.println("Replacing wheels as tread depth is low.");		
	}

}
