package com.anoop.dp.visitor;

public interface CarElement {
	
	public void accept(CarElementVisitor visitor);
	
}
