package com.anoop.dp.visitor;

import java.util.List;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Car implements CarElement {
	
	private final Body body;
	private final Engine engine;
	private final List<Wheel> wheels;
	
	@Override
	public void accept(CarElementVisitor visitor) {
		visitor.visit(body);
		visitor.visit(engine);
		for(Wheel wheel : wheels) {
			visitor.visit(wheel);
		}
	}
}