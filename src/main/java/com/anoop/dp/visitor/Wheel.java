package com.anoop.dp.visitor;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Wheel  implements CarElement {
	
	private final String name;
	
	@Override
	public void accept(CarElementVisitor visitor) {
		visitor.visit(this);
	}

}