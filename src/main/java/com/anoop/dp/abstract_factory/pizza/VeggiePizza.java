package com.anoop.dp.abstract_factory.pizza;

import com.anoop.dp.abstract_factory.ingredients.factory.PizzaIngredientFactory;

public class VeggiePizza extends Pizza {

	private PizzaIngredientFactory ingredientFactory;
	
	public VeggiePizza(PizzaIngredientFactory ingredientFactory) {
		this.ingredientFactory = ingredientFactory;
	}
	
	@Override
	public void prepare() {
		System.out.println("Preparing " + name); 
		dough = ingredientFactory.createDough(); 
		sauce = ingredientFactory.createSauce(); 
		cheese = ingredientFactory.createCheese();
		toppings = ingredientFactory.createVeggies();
	}
	
}
