package com.anoop.dp.abstract_factory.pizza;

import java.util.ArrayList;

import com.anoop.dp.abstract_factory.ingredients.factory.PizzaIngredientFactory;

public class CheesePizza extends Pizza {

	private PizzaIngredientFactory ingredientFactory;
	
	public CheesePizza(PizzaIngredientFactory ingredientFactory) {
		this.ingredientFactory = ingredientFactory;
	}
	
	@Override
	public void prepare() {
		System.out.println("Preparing " + name); 
		dough = ingredientFactory.createDough(); 
		sauce = ingredientFactory.createSauce(); 
		cheese = ingredientFactory.createCheese();
		toppings = new ArrayList<>();//no veggies
	}
	
}
