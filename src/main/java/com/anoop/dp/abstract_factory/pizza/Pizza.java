package com.anoop.dp.abstract_factory.pizza;

import java.util.ArrayList;
import java.util.List;

import com.anoop.dp.abstract_factory.ingredients.Cheese;
import com.anoop.dp.abstract_factory.ingredients.Dough;
import com.anoop.dp.abstract_factory.ingredients.Sauce;
import com.anoop.dp.abstract_factory.ingredients.Vegetable;

public abstract class Pizza {

	protected String name;
	protected Dough dough;
	protected Sauce sauce;
	protected Cheese cheese;
	protected List<Vegetable> toppings = new ArrayList<>();

	public abstract void prepare();/* {
		System.out.println("Preparing " + name);
		System.out.println("Tossing dough "+dough.getClass().getSimpleName());
		System.out.println("Adding sauce "+sauce.getClass().getSimpleName());
		System.out.println("Adding cheese "+cheese.getClass().getSimpleName());
		System.out.println("Adding toppings: ");
		for (Vegetable topping : toppings) {
			System.out.println(" " + topping.getClass().getSimpleName());
		}
	}*/

	public void bake() {
		System.out.println("Bake for 25 minutes at 350");
	}

	public void cut() {
		System.out.println("Cutting the pizza into diagonal slices");
	}

	public void box() {
		System.out.println("Place pizza in official PizzaStore box");
	}

	public String getName() {
		return name;
	}

}
