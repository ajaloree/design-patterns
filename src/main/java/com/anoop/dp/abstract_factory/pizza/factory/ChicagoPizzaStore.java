package com.anoop.dp.abstract_factory.pizza.factory;

import com.anoop.dp.abstract_factory.ingredients.factory.PizzaIngredientFactory;
import com.anoop.dp.abstract_factory.pizza.CheesePizza;
import com.anoop.dp.abstract_factory.pizza.Pizza;
import com.anoop.dp.abstract_factory.pizza.VeggiePizza;

public class ChicagoPizzaStore extends PizzaStore {
	
	private PizzaIngredientFactory ingredientFactory;
	
	public ChicagoPizzaStore(PizzaIngredientFactory ingredientFactory) {
		this.ingredientFactory = ingredientFactory;
	}
	
	@Override
	public Pizza createPizza(String type) {
		
		if(type.equals("cheese")) {
			return new CheesePizza(ingredientFactory);
		} else if(type.equals("veggie")) {
			return new VeggiePizza(ingredientFactory);
		} else {
			return null;
		}
	}

}
