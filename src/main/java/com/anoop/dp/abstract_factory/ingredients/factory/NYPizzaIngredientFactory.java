package com.anoop.dp.abstract_factory.ingredients.factory;

import java.util.List;

import com.anoop.dp.abstract_factory.ingredients.Cheese;
import com.anoop.dp.abstract_factory.ingredients.Dough;
import com.anoop.dp.abstract_factory.ingredients.Sauce;
import com.anoop.dp.abstract_factory.ingredients.Vegetable;
import com.anoop.dp.abstract_factory.ingredients.impl.Capsicum;
import com.anoop.dp.abstract_factory.ingredients.impl.MarinaraSauce;
import com.anoop.dp.abstract_factory.ingredients.impl.Mushroom;
import com.anoop.dp.abstract_factory.ingredients.impl.Olive;
import com.anoop.dp.abstract_factory.ingredients.impl.Parmesan;
import com.anoop.dp.abstract_factory.ingredients.impl.ThinCrustDough;

public class NYPizzaIngredientFactory implements PizzaIngredientFactory {

	@Override
	public Dough createDough() {
		return new ThinCrustDough();
	}

	@Override
	public Sauce createSauce() {
		return new MarinaraSauce();
	}

	@Override
	public Cheese createCheese() {
		return new Parmesan();
	}

	@Override
	public List<Vegetable> createVeggies() {
		return List.of(new Mushroom(), new Capsicum(), new Olive());
	}

}
