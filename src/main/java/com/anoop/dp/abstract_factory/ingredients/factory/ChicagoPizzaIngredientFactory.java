package com.anoop.dp.abstract_factory.ingredients.factory;

import java.util.List;

import com.anoop.dp.abstract_factory.ingredients.Cheese;
import com.anoop.dp.abstract_factory.ingredients.Dough;
import com.anoop.dp.abstract_factory.ingredients.Sauce;
import com.anoop.dp.abstract_factory.ingredients.Vegetable;
import com.anoop.dp.abstract_factory.ingredients.impl.Mozzarella;
import com.anoop.dp.abstract_factory.ingredients.impl.Mushroom;
import com.anoop.dp.abstract_factory.ingredients.impl.Olive;
import com.anoop.dp.abstract_factory.ingredients.impl.Onion;
import com.anoop.dp.abstract_factory.ingredients.impl.ThickCrustDough;
import com.anoop.dp.abstract_factory.ingredients.impl.TomatoAndBasil;

public class ChicagoPizzaIngredientFactory implements PizzaIngredientFactory {

	@Override
	public Dough createDough() {
		return new ThickCrustDough();
	}

	@Override
	public Sauce createSauce() {
		return new TomatoAndBasil();
	}

	@Override
	public Cheese createCheese() {
		return new Mozzarella();
	}

	@Override
	public List<Vegetable> createVeggies() {
		return List.of(new Mushroom(), new Onion(), new Olive());
	}

}
