package com.anoop.dp.abstract_factory.ingredients.factory;

import java.util.List;

import com.anoop.dp.abstract_factory.ingredients.Cheese;
import com.anoop.dp.abstract_factory.ingredients.Dough;
import com.anoop.dp.abstract_factory.ingredients.Sauce;
import com.anoop.dp.abstract_factory.ingredients.Vegetable;

public interface PizzaIngredientFactory {
	
	public Dough createDough();
	public Sauce createSauce();
	public Cheese createCheese(); 
	public List<Vegetable> createVeggies(); 
	
}