package com.anoop.dp.decorator;

public class HorizontalScrollBarDecorator extends WindowDecorator {

	public HorizontalScrollBarDecorator(Window windowToBeDecorated) {
		super(windowToBeDecorated);
	}
	
	@Override
	public void draw() {
		super.draw();
		drawHorizontalScrollBar();
	}
	
	@Override
	public String getDescription() {
		return super.getDescription()+", including Horizontal Scrollbars";
	}
	
	private void drawHorizontalScrollBar() {
		System.out.println("Adding horizontal scrollbars to the window");
	}
}
