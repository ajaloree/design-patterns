package com.anoop.dp.decorator;

public class DecoratorMain {

	public static void main(String[] args) {

		Window sw = new SimpleWindow();
		sw.draw();
		System.out.println(sw.getDescription());

		Window hw = new HorizontalScrollBarDecorator( new SimpleWindow());
		hw.draw();
		System.out.println(hw.getDescription());

		Window vw = new VerticalScrollBarDecorator( new SimpleWindow());
		vw.draw();
		System.out.println(vw.getDescription());

		Window hvw = new HorizontalScrollBarDecorator(new VerticalScrollBarDecorator( new SimpleWindow()));
		hvw.draw();
		System.out.println(hvw.getDescription());

	}

}
