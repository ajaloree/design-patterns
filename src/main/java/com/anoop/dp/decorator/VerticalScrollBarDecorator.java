package com.anoop.dp.decorator;

public class VerticalScrollBarDecorator extends WindowDecorator {

	public VerticalScrollBarDecorator(Window windowToBeDecorated) {
		super(windowToBeDecorated);
	}
	
	@Override
	public void draw() {
		super.draw();
		drawVerticalScrollBar();
	}
	
	@Override
	public String getDescription() {
		return super.getDescription()+", including Vertical Scrollbars";
	}
	
	private void drawVerticalScrollBar() {
		System.out.println("Adding vertical scrollbars to the window");
	}
}
