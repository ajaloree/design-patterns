package com.anoop.dp.decorator;

public abstract class WindowDecorator implements Window {
	
	private final Window windowToBeDecorated;
	
	public WindowDecorator(Window windowToBeDecorated) {
		this.windowToBeDecorated = windowToBeDecorated;
	}
	
	@Override
	public void draw() {
		windowToBeDecorated.draw(); //Delegation
	}

	@Override
	public String getDescription() {
		return windowToBeDecorated.getDescription(); //Delegation
	}

}
