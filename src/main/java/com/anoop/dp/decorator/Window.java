package com.anoop.dp.decorator;

public interface Window {

	public void draw();
	
	public String getDescription();
	
}
