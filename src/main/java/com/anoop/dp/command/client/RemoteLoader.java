package com.anoop.dp.command.client;

import com.anoop.dp.command.concrete_commands.CeilingFanHighCommand;
import com.anoop.dp.command.concrete_commands.CeilingFanLowCommand;
import com.anoop.dp.command.concrete_commands.GarageDoorCloseCommand;
import com.anoop.dp.command.concrete_commands.GarageDoorOpenCommand;
import com.anoop.dp.command.concrete_commands.LightOffCommand;
import com.anoop.dp.command.concrete_commands.LightOnCommand;
import com.anoop.dp.command.invoker.RemoteControl;
import com.anoop.dp.command.receivers.CeilingFan;
import com.anoop.dp.command.receivers.GarageDoor;
import com.anoop.dp.command.receivers.Light;

public class RemoteLoader {

	public static void main(String[] args) {
		
		Light light = new Light("white");
		GarageDoor garageDoor = new GarageDoor();
		CeilingFan fan = new CeilingFan("living_room");
		
		LightOnCommand light_on_command = new LightOnCommand(light);
		LightOffCommand light_off_command = new LightOffCommand(light);
		GarageDoorOpenCommand garage_door_open_command = new GarageDoorOpenCommand(garageDoor);
		GarageDoorCloseCommand garage_door_close_command = new GarageDoorCloseCommand(garageDoor);
		CeilingFanHighCommand fan_high_command = new CeilingFanHighCommand(fan);
		CeilingFanLowCommand fan_low_command = new CeilingFanLowCommand(fan);
		
		RemoteControl rc = new RemoteControl();
		
		rc.setCommand(0, light_on_command, light_off_command);
		rc.setCommand(1, garage_door_open_command, garage_door_close_command);
		rc.setCommand(2, fan_high_command, fan_low_command);
		
		System.out.println("Remote control created.");
		
		rc.onButtonWasPushed(0);
		rc.onButtonWasPushed(1);
		rc.onButtonWasPushed(2);
		
		rc.offButtonWasPushed(0);
		rc.offButtonWasPushed(1);
		rc.offButtonWasPushed(2);
		
		rc.onButtonWasPushed(1);
		rc.undoButtonWasPushed();

	}
}
