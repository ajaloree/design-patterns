package com.anoop.dp.command.receivers;

public class Light {

	private final String colour;
	private Boolean isOn = Boolean.FALSE;
	
	public Light(String colour) {
		this.colour = colour;
	}
	
	public void on() {
		this.isOn = Boolean.TRUE;
		System.out.println("Light "+colour+" is turned on.");
	}

	public void off() {
		this.isOn = Boolean.FALSE;
		System.out.println("Light "+colour+" is turned off.");
	}
	
	public Boolean isOn() {
		return isOn;
	}
}
