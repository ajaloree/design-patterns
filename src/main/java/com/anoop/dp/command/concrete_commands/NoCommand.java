package com.anoop.dp.command.concrete_commands;

import com.anoop.dp.command.Command;

public class NoCommand implements Command {

	@Override
	public void execute() {
		
	}

	@Override
	public void undo() {
		
	}

}