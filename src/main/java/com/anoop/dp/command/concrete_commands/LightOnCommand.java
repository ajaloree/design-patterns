package com.anoop.dp.command.concrete_commands;

import com.anoop.dp.command.Command;
import com.anoop.dp.command.receivers.Light;

public class LightOnCommand implements Command {
	
	private Light light;
	
	public LightOnCommand(Light light) {
		this.light = light;
	}
	
	@Override
	public void execute() {
		light.on();
	}

	@Override
	public void undo() {
		light.off();
	}
	
}