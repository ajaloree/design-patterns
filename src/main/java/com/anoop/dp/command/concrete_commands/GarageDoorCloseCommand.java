package com.anoop.dp.command.concrete_commands;

import com.anoop.dp.command.Command;
import com.anoop.dp.command.receivers.GarageDoor;

public class GarageDoorCloseCommand implements Command {

	private final GarageDoor gd;
	
	public GarageDoorCloseCommand(GarageDoor gd) {
		this.gd = gd;
	}
	
	@Override
	public void execute() {
		gd.down();
		gd.lightOff();
	}
	
	@Override
	public void undo() {
		gd.up();
		gd.lightOn();
	}
}