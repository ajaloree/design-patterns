package com.anoop.dp.command.concrete_commands;

import com.anoop.dp.command.Command;
import com.anoop.dp.command.receivers.Light;

public class LightOffCommand implements Command {
	
	private Light light;
	
	public LightOffCommand(Light light) {
		this.light = light;
	}
	
	@Override
	public void execute() {
		light.off();
	}

	@Override
	public void undo() {
		light.on();
	}

}