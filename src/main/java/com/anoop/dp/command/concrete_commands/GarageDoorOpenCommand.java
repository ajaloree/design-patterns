package com.anoop.dp.command.concrete_commands;

import com.anoop.dp.command.Command;
import com.anoop.dp.command.receivers.GarageDoor;

public class GarageDoorOpenCommand implements Command {

	private final GarageDoor gd;
	
	public GarageDoorOpenCommand(GarageDoor gd) {
		this.gd = gd;
	}
	
	@Override
	public void execute() {
		gd.up();
		gd.lightOn();
	}
	
	@Override
	public void undo() {
		gd.down();
		gd.lightOff();
	}

}