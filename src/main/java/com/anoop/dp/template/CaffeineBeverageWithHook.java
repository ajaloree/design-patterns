package com.anoop.dp.template;

public abstract class CaffeineBeverageWithHook {
	
	final UserInput ui;
	
	public CaffeineBeverageWithHook(UserInput ui) {
		this.ui = ui;
	}
	
	final void prepareRecipe() { 
		boilWater();
		brew(); 
		pourInCup();
		if (customerWantsCondiments()) {
			addCondiments(); 
		}
	}
	
	abstract void brew(); 
	
	abstract void addCondiments();
	
	void boilWater() {
		System.out.println("Boiling water"); 
	}
	
	void pourInCup() {
		System.out.println("Pouring into cup"); 
	}

	boolean customerWantsCondiments() {
		return true; 
	}
	 
}
