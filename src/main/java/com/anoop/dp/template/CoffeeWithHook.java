package com.anoop.dp.template;

public class CoffeeWithHook extends CaffeineBeverageWithHook {
	
	public CoffeeWithHook(UserInput ui) {
		super(ui);
	}
	
	@Override
	void brew() {
		System.out.println("Dripping coffee through filter");
	}

	@Override
	void addCondiments() {
		System.out.println("Adding Lemon and Honey");
	}

	public boolean customerWantsCondiments() {
		String answer = ui.getUserInput();
		if (answer.toLowerCase().startsWith("y")) { 
			return true;
		} else {
			return false; 
		}
	}
}
