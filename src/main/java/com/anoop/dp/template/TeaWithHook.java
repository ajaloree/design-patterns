package com.anoop.dp.template;

public class TeaWithHook extends CaffeineBeverageWithHook {

	public TeaWithHook(UserInput ui) {
		super(ui);
	}
	
	@Override
	void brew() {
		System.out.println("Steeping tea leaves");
	}

	@Override
	void addCondiments() {
		System.out.println("Adding Sugar and Milk");
	}

	public boolean customerWantsCondiments() {
		String answer = ui.getUserInput();
		if (answer.toLowerCase().startsWith("y")) { 
			return true;
		} else {
			return false; 
		}
	}
	
}
