package com.anoop.dp.template;

public class BeverageMachine {

	public static void main(String[] args) {

		UserInput ui = new UserInput();
		CaffeineBeverageWithHook tea = new TeaWithHook(ui);
		CaffeineBeverageWithHook coffee = new CoffeeWithHook(ui);
		
		System.out.println("Making Tea...");
		tea.prepareRecipe();
		
		System.out.println("Making Coffee...");
		coffee.prepareRecipe();
		
	}

}
