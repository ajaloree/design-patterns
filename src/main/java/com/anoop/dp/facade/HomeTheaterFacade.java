package com.anoop.dp.facade;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class HomeTheaterFacade {

	private final Amplifier amplifier;
	private final StreamingPlayer player;
	private final Projector projector;
	private final TheaterLights lights;
	private final Screen screen;
	private final PopcornPopper popper;
	
	public void watchMovie(String movie) {
		System.out.println("Get ready to watch a movie..");
		popper.on();
		popper.pop();
		lights.dim(10);
		screen.down();
		projector.on();
		amplifier.on();
		amplifier.setVolume(5);
		player.on();
		player.play("The Predator");
	}
	
	public void endMovie() {
		System.out.println("Shutting movie theater down...");
		popper.off();
		lights.on();
		screen.up();
		projector.off();
		amplifier.off();
		player.stop();
		player.off();
	}
}
