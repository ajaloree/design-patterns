package com.anoop.dp.facade;

public class PopcornPopper {
	
	public void on() {
		System.out.println("Popcorn popper turned on");
	}
	
	public void off() {
		System.out.println("Popcorn popper turned off");
	}
	
	public void pop() {
		System.out.println("Popcorn popper popping...");
	}

}
