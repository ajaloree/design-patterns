package com.anoop.dp.facade;

public class Projector {
		
	public void on() {
		System.out.println("Projector turned on");
	}
	
	public void off() {
		System.out.println("Projector turned off");
	}
	
	public void tvMode() {
		System.out.println("Projector set to TV mode");
	}
	
	public void wideScreenMode() {
		System.out.println("Projector set to wide screen mode");
	}

}
