package com.anoop.dp.facade;

import java.util.stream.Stream;

public class HomeTheaterTestDrive {

	public static void main(String[] args) {
		
		Amplifier amplifier = new Amplifier();
		StreamingPlayer player = new StreamingPlayer();
		Projector projector = new Projector();
		Screen screen = new Screen();
		TheaterLights lights = new TheaterLights();
		PopcornPopper popper = new PopcornPopper();
		
		HomeTheaterFacade homeTheater = new HomeTheaterFacade(amplifier, player, projector, lights, screen, popper);
		
		homeTheater.watchMovie("The Predator");
		Stream.iterate(1, v -> v+1).forEach(v -> {
			System.out.println();
		});
		homeTheater.endMovie();
		
	}
}
