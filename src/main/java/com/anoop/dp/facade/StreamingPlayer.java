package com.anoop.dp.facade;

public class StreamingPlayer {
	
	public void on() {
		System.out.println("Streaming player turned on");
	}
	
	public void off() {
		System.out.println("Streaming player turned off");
	}
	
	public void pause() {
		System.out.println("Streaming player paused");
	}
	
	public void play(String movie) {
		System.out.println("Streaming player streaming "+movie+" now");
	}
	
	public void setSurroundAudio() {
		System.out.println("Streaming player surround audio turned on");
	}
	
	public void setTwoChannelAudio() {
		System.out.println("Streaming player two channel audio set");
	}
	
	public void stop() {
		System.out.println("Streaming player stopped");
	}

}
