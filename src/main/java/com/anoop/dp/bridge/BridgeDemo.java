package com.anoop.dp.bridge;

import com.anoop.dp.bridge.devices.Device;
import com.anoop.dp.bridge.devices.Radio;
import com.anoop.dp.bridge.devices.TV;
import com.anoop.dp.bridge.remotes.AdvancedRemote;
import com.anoop.dp.bridge.remotes.BasicRemote;

public class BridgeDemo {

	public static void main(String[] args) {

		testDevice(new Radio());
		testDevice(new TV());
		
	}

    private static void testDevice(Device device) {
        
    	System.out.println("Tests with basic remote.");
        BasicRemote basicRemote = new BasicRemote(device);
        basicRemote.power();
        device.printStatus();

        System.out.println("Tests with advanced remote.");
        AdvancedRemote advancedRemote = new AdvancedRemote(device);
        advancedRemote.power();
        advancedRemote.mute();
        device.printStatus();
    }
}
