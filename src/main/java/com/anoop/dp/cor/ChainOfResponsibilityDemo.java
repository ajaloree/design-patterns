package com.anoop.dp.cor;

import static com.anoop.dp.cor.Logger.LogLevel.*;
import static com.anoop.dp.cor.Logger.*;

public class ChainOfResponsibilityDemo {
	
	public static void main(String[] args) {
		
        // Build an immutable chain of responsibility
        Logger logger = consoleLogger(all())
                		.appendNext(emailLogger(FUNCTIONAL_MESSAGE, FUNCTIONAL_ERROR))
                		.appendNext(fileLogger(WARNING, ERROR));

        // Handled by consoleLogger since the console has a LogLevel of all
        logger.message("Entering function ProcessOrder().", LogLevel.DEBUG);
        logger.message("Order record retrieved.", LogLevel.INFO);

        // Handled by consoleLogger and emailLogger since emailLogger implements Functional_Error & Functional_Message
        logger.message("Unable to Process Order ORD1 Dated D1 For Customer C1.", LogLevel.FUNCTIONAL_ERROR);
        logger.message("Order Dispatched.", LogLevel.FUNCTIONAL_MESSAGE);

        // Handled by consoleLogger and fileLogger since fileLogger implements Warning & Error
        logger.message("Customer Address details missing in Branch DataBase.", LogLevel.WARNING);
        logger.message("Customer Address details missing in Organization DataBase.", LogLevel.ERROR);
		
	}

}
